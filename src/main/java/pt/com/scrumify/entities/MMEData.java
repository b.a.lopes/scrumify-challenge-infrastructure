package pt.com.scrumify.entities;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class MMEData {
   @Getter
   @Setter
   private boolean actual;
   
   @Getter
   @Setter
   private BigDecimal Billings;

   @Getter
   @Setter
   private BigDecimal CapitalCharges;
   
   @Getter
   @Setter
   private BigDecimal CCI;
   
   @Getter
   @Setter
   private BigDecimal CCIPercentage;

   @Getter
   @Setter
   private BigDecimal CumulativeCosts;

   @Getter
   @Setter
   private Date date;

   @Getter
   @Setter
   private boolean eac;

   @Getter
   @Setter
   private BigDecimal PercentageComplete;

   @Getter
   @Setter
   private BigDecimal ServiceRevenueAdjustment;

   @Getter
   @Setter
   private BigDecimal SubcontractorLabor;
   
   @Getter
   @Setter
   private BigDecimal TotalContractCosts;
   
   @Getter
   @Setter
   private BigDecimal TotalNonPoCCosts;
   
   @Getter
   @Setter
   private BigDecimal TotalPoCCosts;
   
   @Getter
   @Setter
   private BigDecimal TotalServiceRevenue;
}