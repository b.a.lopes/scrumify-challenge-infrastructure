package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.TimesheetExport;

public interface TimesheetExportService {
   List<TimesheetExport> findByResourcesYearAndMonth(List<String> resources, Integer year, Integer month);
   List<TimesheetExport> findByResourcesYearMonthAndFortnight(List<String> resources, Integer year, Integer month, Integer fortnight);
}