# VM Instances Setup with Terraform

You will use terraform to create few virtual machines and configure the network for them. 

## Get your access Key

To work with terraform you will need a **key** generated in GCP from service account: 
- Go to **IAM & Admin** -> **Service accounts** 
- find and select the service account **J2C Training account**
- On **Keys** tab, create a new key
- Save the file in your laptop (you will use this key in google cloud shell)

### Using the key file terraform example
```
provider "google" {
  credentials = file("file.json")
  project = var.project_id
  region  = var.project_region
  zone    = var.project_zone
}
```
Documentation:  
https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference


## Create VM

Using terraform: 
- All VM’s must follow the **name pattern** *nj-{name}-{type_of_machine}* e.g.: nj-Mayllon-jenkins; nj-Bruno-sonarqube.
- Machine type: e2-medium
- Pick one of the following regions: europe-west1, europe-west6, europe-north1. 
- As **metadata**, the following value must be added when creating VM's: **enable-oslogin=True**
- You need external IP address
- Allow **Http** and **Https** traffic
- VM should only be working in workdays between 8A.M. and 8P.M.
- Use VPC network **j2c-vpn**. Create your own subnet in network.

Documentation:  
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance<br />
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy

### Subnet
The **pattern name** for subnet is: *{name}-subnet*<br />
Use this subnet for all scrumify development<br />
Documentation: 
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork<br />
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork

### Firewall
You will need to open, at least 2 ports for Jenkins(8080) and SonarQube(9000). <br />
Use each firewall rule for applicable VM. <br />
* The **pattern name** for firewall is: *{​​​​​​name}​​​​​​-{​​​​​​service}​​​​​​-port-{​​​​​​port}​​​​​​* e.g: tiago-jenkins-port-8080
* Target tags the same as name
* Source IP ranges: `0.0.0.0/0`
* protocol **TCP**

Documentation: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall

### OsLogin
Don't forget to use the following to grant ssh access to machine:
```
 resource "google_project_iam_binding" "os-login-admin-users" {
  role = "roles/compute.osAdminLogin"
  project = var.project_id
  members = [
    "user:xxxxxxx@accenture.com",
  ]
}
```

Documentation: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_project_iam#google_project_iam_binding

:warning: **If any of the above rules are broken the VM will be removed!!!**